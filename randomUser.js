const randomUserGenerator = {
  data() {
    return {
      userTitle: '',
      firstName: '',
      lastName: '',
      country: '',
      userImage: '',
      email: '',
      loading: false,
      error: {
        isError: false,
        message: ''
      },
    }
  },

  created() {
    // when vue insatnce is created we call the get random user API to populate the app with user details
    this.getUser()
  },

  methods: {
    async getUser () {
      this.toggleLoading()

      try {
        await axios.get('https://randomuser.me/api/')
          .then(res => {
            // desctructure the result in to user
            const [ user ] = res.data.results

            // pass user result to method to set user details
            this.setUser(user)

          }).catch(err => {
            // handle any errors by showing them in the console
            this.error = {
              isError: true,
              message: `Error with reply from API: ${err}`
            }

          }).then(() => {
            // always executed after each GET
            this.toggleLoading()
            // handle loading states and or mesages to be shown

          })
      } catch (err) {
        this.error = {
          isError: true,
          message: `Error whilst trying to call API: ${err}`
        }
      }
    },

    setUser (user) {
      this.userTitle = user.name.title
      this.firstName = user.name.first
      this.lastName = user.name.last
      this.country = user.location.country
      this.userImage = user.picture.large
      this.email = user.email
    },

    toggleLoading () {
      this.loading = !this.loading
    }

  },

  computed: {
    fullname () {
      return `${this.userTitle} ${this.firstName} ${this.lastName}`
    }
  }
}

Vue.createApp(randomUserGenerator).mount('#randomUserDetails')
