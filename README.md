# Vue Random user App

[![Netlify Status](https://api.netlify.com/api/v1/badges/dd403698-e860-4d03-abda-32805acdbdd6/deploy-status)](https://app.netlify.com/sites/vue-random-user-api/deploys)

A simple app that calls the Random User API to load user data in to a card view in the `index.html`.

Uses Vue CDN to load in the Vue library and doesn't need to install any NPM packages etc to showcase a simple demo of using Vue progressibely in an existing website/application.